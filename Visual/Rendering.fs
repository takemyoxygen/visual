﻿module Rendering

open Common
open System.Collections.Generic
open System.IO
open System.Diagnostics

let hierarchycalText line: GraphRenderer<_, _, unit> = (fun graph ->
    let processed = new HashSet<_>()
    let rec loop vertex =
        if not (processed.Contains vertex) then
            processed.Add vertex |> ignore
            graph
            |> Graph.adjacentTo vertex
            |> List.map snd
            |> List.iter (fun v ->
                sprintf "%O -> %O" vertex v |> line
                loop v)
    if graph.Vertices.Count > 0 then graph.Vertices |> Map.toSeq |> Seq.nth 0|> fst |> loop
)

let simple line: GraphRenderer<_, _, unit> = (fun graph ->
    let vertices = Graph.allVertices graph
    line "Vertices:"
    vertices |> sprintf "%+A" |> line
    line "Adjacency:"
    Graph.allEdges graph
    |> List.iter (fun (v1, _, v2) -> sprintf "%O -> %O" v1 v2 |> line)
)

let graphviz file : GraphRenderer<_, _, string> = (fun graph ->
    let generateDot file =
        use writer = new StreamWriter(path=file)
        fprintfn writer "digraph G {"

        graph
        |> Graph.allEdges
        |> List.iter (fun (v1, _, v2) -> fprintfn writer "    \"%O\" -> \"%O\";" v1 v2)

        fprintfn writer "}"

    let generateImage dotFile imageFile =
       let cmd = @"C:\Program Files (x86)\Graphviz2.38\bin\dot.exe"
       let args = sprintf "-Tpng -o\"%s\" \"%s\"" imageFile dotFile
       Process.Start(cmd, args).WaitForExit()

    let dotfile = file + ".dot"

    generateDot dotfile
    generateImage dotfile file

    file
)