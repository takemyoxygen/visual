﻿module Common

type Graph<'vertex, 'edge when 'vertex: comparison> = {
    Vertices: Map<'vertex, int>;
    Adjacency: ('edge option) [,]
}

type GraphProvider<'context, 'vertex, 'edge when 'vertex: comparison> = 'context -> Graph<'vertex, 'edge>
type GraphRenderer<'vertex, 'edge, 'result when 'vertex: comparison> = Graph<'vertex, 'edge> -> 'result