﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.

#I @"..\packages\Nuget.Core.2.8.3\lib\net40-Client"
#r "NuGet.Core.dll"
#r "System.Xml.Linq"

#load "Common.fs"
#load "Graph.fs"
#load "Nuget.fs"
#load "Rendering.fs"

open System
open System.IO
open System.Diagnostics
open NuGet

let flow = Nuget.provider Nuget.NugetConfig.defaultConfig
           >> Rendering.graphviz (Path.Combine(__SOURCE_DIRECTORY__, "output.png"))
           >> Process.Start

flow {Package = "Core"; Version = Nuget.Latest}