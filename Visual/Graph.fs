﻿module Graph

open Common

let private ensureVertex v graph name =
    if not (graph.Vertices |> Map.containsKey v) then invalidArg name "Vertex is not in the graph"

let private coordinateOf v graph = graph.Vertices.[v]
let private coordinatesOfPair v1 v2 graph = 
    (coordinateOf v1 graph), (coordinateOf v2 graph)

let empty<'vertex, 'edge when 'vertex: comparison> : Graph<'vertex, 'edge> = 
    {Vertices = Map.empty; Adjacency = Array2D.zeroCreate 0 0}

let hasVertex v graph = graph.Vertices |> Map.containsKey v

let allVertices graph =
    let vertices = Array.zeroCreate graph.Vertices.Count
    graph.Vertices |> Map.toSeq |> Seq.iter (fun (vertex, index) -> vertices.[index] <- vertex)
    vertices

let allEdges graph =
    let vertices = allVertices graph
    let dec x = x - 1
    seq {
        for i in [0.. graph.Adjacency |> Array2D.length1 |> dec] do
            for j in [0.. graph.Adjacency |> Array2D.length2 |> dec] do
                if graph.Adjacency.[i, j].IsSome then yield vertices.[i], graph.Adjacency.[i, j].Value, vertices.[j]
    } |> List.ofSeq

let add vertex graph =
    if (hasVertex vertex graph) then graph
    else
        printfn "Adding vertex %O" vertex
        let size = graph.Vertices.Count
        let vertices = graph.Vertices |> Map.add vertex size
        let adjacency = Array2D.init vertices.Count vertices.Count (fun i j -> 
            if (i < size && j < size) then graph.Adjacency.[i, j] else None)
        {Vertices = vertices; Adjacency = adjacency}

let connect v1 v2 edge graph =
    printfn "Adding edge %O -> %O" v1 v2
    ensureVertex v1 graph "v1"
    ensureVertex v2 graph "v1"

    let i, j = coordinatesOfPair v1 v2 graph

    let adjacency' = Array2D.copy graph.Adjacency
    adjacency'.[i, j] <- Some edge
    {graph with Adjacency = adjacency'}

let getEdge v1 v2 graph =
    ensureVertex v1 graph "v1"
    ensureVertex v2 graph "v2"
    let i, j = coordinatesOfPair v1 v2 graph
    graph.Adjacency.[i, j]

let areConnected v1 v2 = getEdge v1 v2 >> Option.isSome

let adjacentTo v graph =
    ensureVertex v graph "v"
    let vertices = allVertices graph
    let i = coordinateOf v graph

    graph.Adjacency.[i, 0..]
    |> Seq.mapi (fun i d -> Option.map(fun edge -> edge, vertices.[i]) d)
    |> Seq.filter Option.isSome
    |> Seq.map Option.get
    |> List.ofSeq
    