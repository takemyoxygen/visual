﻿module Nuget

open NuGet
open Common
open System.Collections.Generic
open System.IO

module Sources =
    open System
    open System.IO

    let nuget = "https://www.nuget.org/api/v2/"
    let localCache = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "NuGet", "Cache")

type Package = {Name: string; Dependencies: Package list}

type NugetConfig = 
    {Source: string; AllowPrerelease: bool; UseCache: bool; DownloadToCache: bool}
    with static member defaultConfig = {Source = Sources.nuget; AllowPrerelease = true; UseCache = true; DownloadToCache = false}

type RequestedVersion =
    | Latest
    | Specific of version: string

type Request = {Package: string; Version: RequestedVersion}

let private createRepository config = 
    let remote = PackageRepositoryFactory.Default.CreateRepository(config.Source)
    if config.UseCache then
        let local = PackageRepositoryFactory.Default.CreateRepository(Sources.localCache)
        new AggregateRepository([local; remote]) :> IPackageRepository
    else remote

let pathToLocallyCached (package: IPackage) = 
    let file = package.Id + "." + package.Version.ToString() + Constants.PackageExtension
    Path.Combine(Sources.localCache, file)

let download (package: IPackage) =
    let filename = pathToLocallyCached package
    if not (File.Exists filename) then
        printfn "[Nuget] Downloading package to \"%s\"" filename
        use target = File.Create(filename)
        let content = package.GetStream().ReadAllBytes()
        target.Write(content, 0, content.Length)

let resolveRoot request config (repository: IPackageRepository) = 
    match request.Version with
    | Latest -> repository.FindPackage(request.Package)
    | Specific(version) -> repository.FindPackage(request.Package, SemanticVersion.Parse(version), config.AllowPrerelease, false)

let dependenciesOf config request =
    let repository = createRepository config
    let root = resolveRoot request config repository
    let cache = new Dictionary<_, _>()

    let rec findDependencies (package: IPackage) =
        match cache.TryGetValue(package.GetFullName()) with
        | true, p -> p
        | false, _ ->
            printfn "[Nuget] Resolving dependencies of \"%s\"" (package.GetFullName())
            if config.DownloadToCache then download package
            let dependencies = package.DependencySets
                               |> Seq.collect (fun set -> set.Dependencies)
                               |> Seq.map (fun dep -> repository.ResolveDependency(dep, config.AllowPrerelease, false))
                               |> Seq.filter (fun p -> p <> null && p.Id <> null)
                               |> Seq.map findDependencies
                               |> List.ofSeq
            let result = {Name = package.GetFullName(); Dependencies = dependencies}
            cache.Add(package.GetFullName(), result)
            result

    findDependencies root


let asGraph root =
    let rec loop package graph =
        Graph.add package.Name graph
        |> List.fold (fun g p -> g |> Graph.add p.Name |> Graph.connect package.Name p.Name () |> loop p)
        <| package.Dependencies

    loop root Graph.empty 


let provider config: GraphProvider<Request, string, unit> = dependenciesOf config >> asGraph